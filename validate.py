import torch as t
import torch.cuda
import argparse
import os
import sys
import cv2
import re
import model.network as net
import model.loss as loss
import model.data as dat
import torch.nn as nn
from torch.utils import data
import torch.optim as optim
from torch.autograd import Variable
import time
import numpy as np

def parse_args():
    parser = argparse.ArgumentParser(description='train a network')
    parser.add_argument('--dataset',help='training set config file',default='dataset/kitti _valid.data',type=str)
    parser.add_argument('--netcfg',help='the network config file',default='cfg/complex-yolo.cfg',type=str)
    parser.add_argument('--weight',help='the network weight file',default='backup/complex-yolo.backup',type=str)
    parser.add_argument('--init',help='initialize the network parameter',default=0,type=int)
    parser.add_argument('--cuda',help='use the GPU',default=1,type=int)
    parser.add_argument('--ngpus',help='use mult-gpu',default=1,type=int)
    args = parser.parse_args()
    return args

def parse_dataset_cfg(cfgfile):
    with open(cfgfile,'r') as fp:
        p1 = re.compile(r'classes=\d')
        p2 = re.compile(r'train=')
        p3 = re.compile(r'names=')
        p4 = re.compile(r'backup=')
        for line in fp.readlines():
            a = line.replace(' ','').replace('\n','')
            if p1.findall(a):
                classes = re.sub('classes=','',a)
            if p2.findall(a):
                trainlist = re.sub('train=','',a)
            if p3.findall(a):
                namesdir = re.sub('names=','',a)
            if p4.findall(a):
                backupdir = re.sub('backup=','',a)
    return int(classes),trainlist,namesdir,backupdir

def parse_network_cfg(cfgfile):
    with open(cfgfile,'r') as fp:
        layerList = []
        layerInfo = ''
        p = re.compile(r'\[\w+\]')
        p1 = re.compile(r'#.+')
        for line in fp.readlines():
            if p.findall(line):
                if layerInfo:
                    layerList.append(layerInfo)
                    layerInfo = ''
            if line == '\n' or p1.findall(line):
                continue
            line = line.replace(' ','')
            layerInfo += line
        layerList.append(layerInfo)
    print('layer number is %d'%(layerList.__len__() - 1) )
    return layerList

if __name__ == '__main__':
	args = parse_args()
	print(args)
	classes, trainlist, namesdir, backupdir = parse_dataset_cfg(args.dataset)
	print('%d classes in dataset'%classes)
	print('trainlist directory is ' + trainlist)
	#step 1: parse the network
	layerList = parse_network_cfg(args.netcfg)
	netname = args.netcfg.split('.')[0].split('/')[-1]
	layer = []
	print('the depth of the network is %d'%(layerList.__len__()-1))
	network = net.network(layerList)
	max_batch = network.max_batches
	batch = network.batch
	lr = network.lr / batch
	#step 2: load network parameters
	if args.init == 0:
		network.load_weights(args.weight)
	else:
		network.init_weights()
	seen = 0 #since there are only one eval ephoc, seen starts from zero allways
	criterion = loss.CostYoloV2(network.layers[-1].flow[0], seen)
	print('seen=%d'%seen)							
	start_batch = seen / batch
	layerNum = network.layerNum
	if args.cuda:
		if args.ngpus:
		    print('use mult-gpu')
		    network = nn.DataParallel(network).cuda()   
		    model = network.module
		    num_gpu = torch.cuda.device_count()
		else:
		    network = network.cuda()
		    model = network
		    num_gpu = 1
	#step 3: load data 
	dataset = dat.YoloDataset(trainlist,416,416)
	timesPerEpoch = int(dataset.len / batch)
	max_epoch = int(max_batch / timesPerEpoch)
	print('Batch quantity : %d'%timesPerEpoch)
	start_epoch = int(start_batch / timesPerEpoch)
	dataloader = data.DataLoader(dataset, batch_size=batch, shuffle=1, drop_last=True)
	#step 5: start evaluation
	print('start valuation...')
	t_start = time.time()
        iou_epoch = 0.0
        class_epoch = 0.0
        obj_epoch = 0.0
        for ii,(imgs, labels) in enumerate(dataloader):
            imgs = Variable( imgs )
            labels =  Variable(labels)
            if args.cuda:
                imgs =  imgs.cuda()
                #labels =  labels.cuda()
            #forward propagate
            t0 = time.time()
            pred = network.forward(imgs)
            #calculate loss
            t1 = time.time()
            pred = pred.cpu()
            cost = criterion(pred, labels)
            cost = cost.cuda()
            t2 = time.time()
            print('forward time: %f, loss time: %f'%((t1-t0),(t2-t1)))
            loss = criterion.loss.cpu().data.view(1)	
            iou_epoch += criterion.Aiou.cpu().data.view(1)
            class_epoch += criterion.AclassP.cpu().data.view(1)
            obj_epoch += criterion.Aobj.cpu().data.view(1)
            if np.isnan(loss.numpy()):
                print('loss is nan, check parameters!')
                sys.exit(-1)
        iou_epoch = iou_epoch / timesPerEpoch
        class_epoch = class_epoch / timesPerEpoch
        obj_epoch = obj_epoch / timesPerEpoch
	print('IOU: %f, Class Ac: %f, Obj Ac: %f'%(iou_epoch,class_epoch,obj_epoch))
	print('finished training!')
