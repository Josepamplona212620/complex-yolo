# This file is used for testing. After obtaining the image file generated by the radar data bin file, use this script to extract and convert the object coordinates recorded by the camera's txt file into image coordinates and draw them in the generated image file. # How can I box correctly, then there is no problem with the conversion.
import os 
import os.path
import math
import numpy as np
import pandas as pd
import cv2

def print_bbox(b_box,img_file,lbl_file,width = 80,range_l = 80):
	img = cv2.imread(img_file)
	for l in b_box:
		x = l[0]*1024
		z = l[1]*1024
		w = l[2]*1024
		le = l[3]*1024
		fy = -l[4]
		alpa = math.atan(w/le)
		# Calculate the four vertex coordinates of a rectangle
		q = np.sqrt(math.pow(w,2) + math.pow(le,2))/2
		p = fy - alpa
		k = fy + alpa
		x1 = int(q*math.sin(p) + z + 0.27)
		y1 = int(q*math.cos(p) + x)
		x2 = int(z - q*math.sin(p))
		y2 = int(x - q*math.cos(p))
		x3 = int(q*math.sin(k) + z + 0.27)
		y3 = int(q*math.cos(k) + x)
		x4 = int(z - q *math.sin(k))
		y4 = int(x - q*math.cos(k))
		x5 = int(z + 0.27)
		y5 = int(x)
		# Four vertex connection
		img = cv2.line(img,(y1,x1),(y3,x3),(255,255,0))
		img = cv2.line(img,(y3,x3),(y2,x2),(255,255,0))
		img = cv2.line(img,(y2,x2),(y4,x4),(255,255,0))
		img = cv2.line(img,(y4,x4),(y1,x1),(255,255,0))
		img = cv2.line(img,(int((y1+y3)/2),int((x1+x3)/2)),(y5,x5),(255,255,0))

	lines = open(lbl_file).readlines()
	for line in lines:
	    line = line.replace('\n', '')
	    l = line.split(' ')
	    # ignore Dontcare
	    if l[0] == "DontCare":
		continue
	    else:
		#df = pd.DataFrame({'class':l[0],'data':l[1:14]})# Convert list to form
		l =np.asarray(l[1:15:1]).astype(float)# Convert str to a numeric type
		z = l[0]*1024
		x = l[1]*1024
		w = l[2]*1024
		le = l[3]*1024
		fy = -l[4]
		alpa = math.atan(w/le)
		# Calculate the four vertex coordinates of a rectangle
		q = np.sqrt(math.pow(w,2) + math.pow(le,2))/2
		p = fy - alpa
		k = fy + alpa
		x1 = int(q*math.sin(p) + z)
		y1 = int(q*math.cos(p) + x)
		x2 = int(z - q*math.sin(p))
		y2 = int(x - q*math.cos(p))
		x3 = int(q*math.sin(k) + z)
		y3 = int(q*math.cos(k) + x)
		x4 = int(z - q *math.sin(k))
		y4 = int(x - q*math.cos(k))
		x5 = int(z + 0.27)
		y5 = int(x)
		# Four vertex connection
		img = cv2.line(img,(y1,x1),(y3,x3),(0,255,255))
		img = cv2.line(img,(y3,x3),(y2,x2),(0,255,255))
		img = cv2.line(img,(y2,x2),(y4,x4),(0,255,255))
		img = cv2.line(img,(y4,x4),(y1,x1),(0,255,255))
		img = cv2.line(img,(int((y1+y3)/2),int((x1+x3)/2)),(y5,x5),(0,255,255))

	cv2.imshow("img",img)
	cv2.waitKey(0)




        

               
             
