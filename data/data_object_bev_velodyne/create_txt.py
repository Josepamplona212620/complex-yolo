import os
import glob
base_dir=os.getcwd()
train_dir=os.path.join(base_dir,'Train/labels')
valid_dir=os.path.join(base_dir,'Valid/labels')
t=open("train.txt","w+")
for fn in os.listdir(train_dir):
	t_dir=(os.path.join(train_dir,fn)+"\n")
	t_dir = t_dir.replace('labels','images').replace('.txt','.png')
	t.write(t_dir)
t.close()
v=open("valid.txt","w+")
for fn in os.listdir(valid_dir):
	v_dir=(os.path.join(valid_dir,fn)+"\n")
	v_dir = v_dir.replace('labels','images').replace('.txt','.png')
	v.write(v_dir)
v.close()

