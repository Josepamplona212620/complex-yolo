'''
This file is used to convert the camera coordinate point txt file to the label label required for the training network.
'''
import os 
import os.path
import math
import numpy as np
import pandas as pd
import cv2

width = 80
range_l = 80
# File open and save path
file_path = "/home/jose/Codigos/VoxelNet-tensorflow/data/object/testing/label_2/"
save_path = '/home/jose/Codigos/complex-yolo/data/data_object_bev_velodyne/Valid/labels/'
files = os.listdir(file_path)
# Loop through files
for labelfile in files:
    # An empty array of saved data
    bx = []
    by = []
    bw = []
    bl = []
    bc = []
    bf = []
    bm = []
    be = []
    filename = os.path.split(labelfile)[-1].split('.')[0]    
    lines = open(os.path.join(file_path + labelfile)).readlines()
    for line in lines:
        line = line.replace('\n', '')
        l = line.split(' ')
        if l[0] == "DontCare" :
            continue
        else:
            class_name = l[0]# Convert class to 8 classification flags corresponding to 0, 1, ...
            if class_name == 'Car':
                class_name = '0'
            elif class_name == 'Van':
                class_name = '1'
            elif class_name == 'Truck':
                class_name = '2'
            elif class_name == 'Pedestrian':
                class_name = '3'
            elif class_name == 'Person_sitting':
                class_name = '4'
            elif class_name == 'Cyclist':
                class_name = '5'
            elif class_name == 'Tram':
                class_name = '6'
            else:
                class_name = '7'
            df = pd.DataFrame({'class':l[0],'data':l[1:15:1]})
            l_new = np.asarray(l[1:15:1]).astype(float)# Convert str to numeric
            # Get the camera coordinate system from str
            x = l_new[10]
            y = l_new[11]
            z = l_new[12]
            w = l_new[8]
            le = l_new[9]
            ry = l_new[13]
            fy = - ry
            # Convert camera coordinate system to radar coordinate system
            v_x = z + 0.27
            v_y = - x 
            if 0 < z <(range_l-0.27) and  -(width/2)< x < (width/2) and -1.25 < y <2 :# Select a point that meets the criteria in the camera coordinate system and calculate the normalized value
                bc.append(class_name)
                im_x = (1023-((v_y + (width/2))*1024/width))/1024
                by.append(im_x)
                im_y = (1023-(v_x * 1024/range_l))/1024
                bx.append(im_y)
                im_w = w/width
                bw.append(im_w)
                im_l = le/range_l
                bl.append(im_l)
                im_fy = fy
                bf.append(im_fy)
                lm = math.sin(im_fy)
                bm.append(lm)
                re = math.cos(im_fy)
                be.append(re)  
            else:
                continue
    # Open a file that cyclically saves data
    save_file = open(os.path.join(save_path + filename + '.txt'),'w')
    unuse_file = open("/home/jose/Codigos/complex-yolo/data/data_object_bev_velodyne/Valid/labels/unuse_label.txt",'a+')# Empty label file save path
    size = len(bc)# Get the number of file lines
    if size == 0 :# If the file is empty, save the file name to unuse_file
        print(filename +' ' + "no class")
        unuse_file.write(filename +'\n')
    else:# If the file is not empty, the data is saved in a row, in the order class,x,y,w,l,fy,im,ie
        for index in range(size):
            temp = bc[index] + ' ' + str(bx[index]) + ' ' + str(by[index]) + ' '+ str(bw[index]) + ' ' + str(bl[index]) + ' ' + str(bf[index]) + ' ' + str(bm[index]) + ' ' + str(be[index])
            save_file.write(temp + '\n')
save_file.close()
unuse_file.close()


       
        
       
