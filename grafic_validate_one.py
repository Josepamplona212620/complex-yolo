import torch as t
import torch.cuda
import argparse
import os
import sys
import cv2
import re
import model.network as net
import model.eval_loss as loss
import model.data as dat
import torch.nn as nn
from torch.utils import data
import torch.optim as optim
from torch.autograd import Variable
import time
import numpy as np
from show_validation import print_bbox
anchors =  [0.57273, 0.677385, 1.87446, 2.06253, 3.33843, 5.47434, 7.88282, 3.52778, 9.77052, 9.16828]

def parse_args():
    parser = argparse.ArgumentParser(description='train a network')
    parser.add_argument('--dataset',help='training set config file',default='dataset/kitti.data',type=str)
    parser.add_argument('--netcfg',help='the network config file',default='cfg/complex-yolo.cfg',type=str)
    parser.add_argument('--weight',help='the network weight file',default='backup/complex-yolo.backup',type=str)
    parser.add_argument('--init',help='initialize the network parameter',default=0,type=int)
    parser.add_argument('--cuda',help='use the GPU',default=1,type=int)
    parser.add_argument('--ngpus',help='use mult-gpu',default=1,type=int)
    args = parser.parse_args()
    return args

def parse_dataset_cfg(cfgfile):
    with open(cfgfile,'r') as fp:
        p1 = re.compile(r'classes=\d')
        p2 = re.compile(r'train=')
        p3 = re.compile(r'names=')
        p4 = re.compile(r'backup=')
        for line in fp.readlines():
            a = line.replace(' ','').replace('\n','')
            if p1.findall(a):
                classes = re.sub('classes=','',a)
            if p2.findall(a):
                trainlist = re.sub('train=','',a)
            if p3.findall(a):
                namesdir = re.sub('names=','',a)
            if p4.findall(a):
                backupdir = re.sub('backup=','',a)
    return int(classes),trainlist,namesdir,backupdir

def parse_network_cfg(cfgfile):
    with open(cfgfile,'r') as fp:
        layerList = []
        layerInfo = ''
        p = re.compile(r'\[\w+\]')
        p1 = re.compile(r'#.+')
        for line in fp.readlines():
            if p.findall(line):
                if layerInfo:
                    layerList.append(layerInfo)
                    layerInfo = ''
            if line == '\n' or p1.findall(line):
                continue
            line = line.replace(' ','')
            layerInfo += line
        layerList.append(layerInfo)
    print('layer number is %d'%(layerList.__len__() - 1) )
    return layerList

if __name__ == '__main__':
	args = parse_args()
	print(args)
	classes, trainlist, namesdir, backupdir = parse_dataset_cfg(args.dataset)
	print('%d classes in dataset'%classes)
	print('trainlist directory is ' + trainlist)
	#step 1: parse the network
	layerList = parse_network_cfg(args.netcfg)
	netname = args.netcfg.split('.')[0].split('/')[-1]
	layer = []
	print('the depth of the network is %d'%(layerList.__len__()-1))
	network = net.network(layerList)
	max_batch = network.max_batches
	batch = 1 #Number of images to analize
	lr = network.lr / batch
	#step 2: load network parameters
	network.load_weights(args.weight)
	seen = 0 #since there are only one eval ephoc, seen starts from zero allways
	criterion = loss.CostYoloV2(network.layers[-1].flow[0], seen)
	print('seen=%d'%seen)		
	layerNum = network.layerNum
	if args.cuda:
		if args.ngpus:
		    print('use mult-gpu')
		    network = nn.DataParallel(network).cuda()   
		    model = network.module
		    num_gpu = torch.cuda.device_count()
		else:
		    network = network.cuda()
		    model = network
		    num_gpu = 1
	#step 3: load data 
	dataset = dat.YoloDataset(trainlist,416,416)

	
	for example in range (10,20):
		#step 5: start evaluation
		print('start evaluation...')
		img_file=dataset.imageList[example]
		lbl_file=dataset.labelList[example]
		print (lbl_file)
		t_start = time.time()
		imgs,labels=dataset[example]
		img = torch.from_numpy(np.expand_dims(imgs,axis=0)).float()
		labels = torch.from_numpy(np.expand_dims(labels,axis=0)).float()
		imgs = Variable( img )
		if args.cuda:
			imgs =  imgs.cuda()
			#labels =  labels.cuda()
		#forward propagate
		t0 = time.time()
		pred = network.forward(imgs)
		#calculate loss
		t1 = time.time()
		pred = pred.cpu()
		cost = criterion(pred, labels)
		cost = cost.cuda()
		t2 = time.time()
		print('forward time: %f, loss time: %f'%((t1-t0),(t2-t1)))
		print('Loss: '% criterion.loss.cpu().data.view(1))	
		print('IOU: %f'% criterion.Aiou.cpu().data.view(1))
		print('Class Ac: %f'% criterion.AclassP.cpu().data.view(1))
		print('Obj Ac: %f'% criterion.Aobj.cpu().data.view(1))

	
		batch_size, channels, in_height, in_width = pred.size()
	 	x_b = pred[0, :, :, :]
		pred_iou=x_b[[6, 21, 36, 51, 66],:,:].data.numpy()
		best_idx=ind = np.unravel_index(np.argsort(pred_iou, axis=None)[::-1], pred_iou.shape)
	
		b_box=[]
		objectness=1
		f=0
		while objectness>0.999:
			box_pred_shift=[0,0,0,0,0] 		
			n=best_idx[0][f] 
			i=best_idx[1][f]
			j=best_idx[2][f] 
			box_pred=(x_b[n*15:n*15+7, i, j].view(7))
			objectness=box_pred[6].view(1).data.numpy()
			box_pred = box_pred.clone()
			box_pred_shift[0] = (1-box_pred[0]).add(i).div(in_width).data.numpy()
			box_pred_shift[1] = box_pred[1].add(j).div(in_height).data.numpy()
			box_pred_shift[2] = (torch.exp(box_pred[2]) * (anchors[n*2]/in_width)).data.numpy()
			box_pred_shift[3] = (torch.exp(box_pred[3]) * (anchors[n*2 + 1]/in_height)).data.numpy()
			box_pred_shift[4] = np.arctan2(x_b[n*15+4, i, j].view(1).data.numpy(),x_b[n*15+5, i, j].view(1).data.numpy())
			b_box.append(box_pred_shift)
			f=f+1
		print_bbox(b_box,img_file,lbl_file)


		print('finished validation!')
